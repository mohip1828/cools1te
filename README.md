# Cools1te

## What is this?

This is a collection of 62 different games, spanning from old flash games to new and great games. 

## How to use?

Simply download the repository. A lot of games don't work locally, so you'll need to either host it yourself using something like Apache2 or use an online service like Glitch or Vercel.